"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request, Response,Flask,redirect,url_for,render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from datetime import datetime as datetime
import dateutil.parser
import time
from pymongo import MongoClient

import logging
logging.basicConfig(foramt='%(levelname)s:%(message)2',
                    level=logging.INFO)
log = logging.getLogger(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.calcdb


###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', type=int)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    date = request.args.get('seconds', type=str)
    time = request.args.get('time',type=str)
    values = datetime.strptime(date, "%Y-%m-%d").timestamp()
    date = int(values) + int(time)
    date = datetime.utcfromtimestamp(date).isoformat()
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, distance, date)
    close_time = acp_times.close_time(km, distance, date)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route('/new', methods=['POST'])
def new():
    """
    item_doc = {
        'name': request.form['name'],
        'description': request.form['description']
    }
    db.tododb.insert_one(item_doc)`
    """
    print("Submit should come overe here")
    log.info("I need it to come here")
    db.calcdb.delete_many({})
    data = request.form #getting a bunch of stuff here information
    close = request.form.getlist("closetimes[]", type=str)
    openlist = request.form.getlist("opentimes[]", type=str)
    km = request.form.getlist("kilo[]", type=str)
    miles = request.form.getlist("miles[]", type=str)
    loop = True
    index = 0 
    while loop:
        if km[index] == "": # if nothing in the database then break
            break
        #writing for the display page and filling in paramaters as we go along the database
        items= {
            'km': "kilometers: " +str(km[index]),
            'mile': "miles: " +str(miles[index]),
            'open': "Opening Time: " +str(openlist[index]),
            'close': "Closing Time: " +str(close[index]) #adding stuff to database making move
        }
        log.info(items)
        db.calcdb.insert_one(items) #inserting database into db 
        index += 1 #in the case that user enters more stuff into loop
    response = Response(status = 200)
    return response #submit is succesful !!!! :)

@app.route("/display") #Display the Database!!!
def todo():
    _items = db.calcdb.find()
    log.info(_items) #basically the same as the given todo function
    items = [item for item in _items]
    log.info(items)
    db.calcdb.delete_many({})
    return render_template('todo.html', items = items)


# Create routes
# Another way, without decorators

#############
app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)
if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
